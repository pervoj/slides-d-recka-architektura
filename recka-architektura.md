---
marp: true
footer: "&copy; 2023 Vojtěch Hrubeš, Vojtěch Perník"
style: |
    @import url("https://rsms.me/inter/inter.css");

    section {
        justify-content: flex-start;
        font-family: "Inter", sans-serif;
    }

    h1 {
        color: inherit;
        font-weight: 500;
        margin-block-end: 0.4em;
    }

    h1::after {
        content: "";
        display: block;
        height: 0.1em;
        width: 1.6em;
        margin-block-start: 0.08em;
        background: #e5a50a;
    }

    a {
        color: #ae7b03;
        text-decoration: underline;
    }

    section.main-title {
        justify-content: center;
        align-items: center;
        text-align: center;
    }

    section.section-title {
        justify-content: center;
    }

    section.main-title h1,
    section.section-title h1 {
        font-size: 2em;
        margin-block-end: 0.2em;
    }

    section.main-title h1::after,
    section.section-title h1::after {
        margin-block-start: 0.4em;
    }

    section.main-title h1::after {
        margin-inline: auto;
    }

    section.image {
        padding: 0;
        flex-direction: row;
        align-items: flex-end;
    }

    section.image > * {
        flex: 1;
    }

    section.image footer {
        display: none;
    }

    section.image h1 {
        color: white;
        background-color: #00000099;
        line-height: 1;
        margin: 0;
        padding: 0.4em 0.6em;
        font-size: 0.8em;
        font-weight: 400;
    }

    section.image h1::after {
        display: none;
    }
---

<!--
_class: main-title
_footer: "CC BY-SA 4.0"
-->

# Řecká architektura

Vojtěch Hrubeš
Vojtěch Perník

<!--
Architekturu starověkého Řecka dělíme do tří základních období: archaické, klasické a helénistické.

Starší stavby pochází z dob mykénské civilizace:

Mezi nejznámější patří mohutné hradby z kyklopského zdiva se Lví bránou v Mykénách.
-->

---

# Základní informace

* 3 stavební řády
    - dórský
    - iónský
    - korintský
* stavby přesně propočítány
* optické klamy
* nepoužívali žádné pojivo
* značný dopad na vývoj architektury

![bg right:54% 100%](./images/navrh.jpg)

<!--
V archaickém období začaly postupně vznikat tři řecké stavební řády: dórský, iónský a korintský.

Řečtí architekti stavěli podle přísných matematických pravidel a propočítávali proporce stavby.

Často také využívali optické klamy. (např. vypouklá podlaha, aby prostor vypadal větší)

Při stavbě na sebe pokládali kamenné bloky bez jakéhokoli pojiva. Spoléhali na hmotnost stavby a přesné spáry. Občas také používali kovové skoby.

Řekové ovlivnili další vývoj architektury ve světě.
-->

---

<!--
_class: section-title
-->

# Stavební řády

---

# Dórský řád

* používán od 7. stol. př. n. l.
* rozšířen na pevninském Řecku
* sloupy
    * mocné, mohutné
    * bez zdobení
    * stavěny z tamburů
    * svislé žlábkování (kanelování)
* chrámy – kladí, sloupy, podnož

![bg right:40% 60%](./images/dorsky-schema.jpg)

<!--
Dórský sloh byl poprvé použit v 7. století před naším letopočtem. Nejvíce byl rozšířen na pevninském Řecku.

Jeho tvar byl odvozen z proporcí mužského těla.

Charakterictický je svými silnými a mocnými sloupy bez zdobených hlavic.

Sloupy byly vystavěny z asi metrových částí, takzvaných tamburů.

Svislé žlábkování (kanelování) podobné sloupům v egyptské Sakkáře.

Chrámy rozděleny do tří hlavních částí: kladí (které nese štít), sloupy a podnož.

Výplň štítu (tympanon) byla často zdobena různými sochami či reliéfy.
-->

---

<!--
_class: image
-->

![bg](./images/parthenon.jpg)

# Parthenón – Akropolis v Athénách, Řecko

<!--
- nejznámější řecká stavba

- převážně dórský s některými iónskými prvky

- součástí Athénské akropole
-->

---

<!--
_class: image
-->

![bg](./images/parthenon-2.jpg)

# Parthenón – Akropolis v Athénách, Řecko

---

# Iónský řád

* používán od 6. stol. př. n. l.
* rozšířen na Egejských ostrovech a v Malé Asii
* elegantnější a dekorativnější než dórský řád
* sloupy štíhlé, s patkou
* hlavice se závitnicí (volutou)
* stavěn soudobě s dórským slohem

![bg right:20% 55%](./images/ionsky-schema.jpg)

<!--
Iónský řád vznikl v 6. století před naším letopočtem a byl hojně rozšířený na Egejských ostrovech a v koloniích Malé Asie.

Je elegantnější a dekorativnější než dórský sloh. Jeho tvar měl symbolizovat ženské tělo.

Sloupy byli štíhlejší a (narozdíl od dóského slohu) měli patku. Sloup byl ukončen zdobenou hlavicí se závitnicí (volutou).

Iónský sloh byl stavěn paralelně s dórským slohem.
-->

---

<!--
_class: image
-->

![bg](./images/apollonuv-chram.jpg)
![bg](./images/apollonuv-chram-2.jpg)

# Apollonův chrám – Didyma, Turecko

---

# Korintský řád

* později než dórský a iónský
* v Řecku zřídka, rozšířen spíše v Římě
* sloupy nejzdobnější

![bg right:20% 60%](./images/korintsky-schema.jpg)

<!--
Korintské stavby se stavěly později než dórské a iónské. V Řecku byl používán zřídka, spíše až později v Římě.

Hlavice sloupů byla ze všech Řeckých řádů nejzdobnější.

Podle legendy měl autor slohu za úkol najít nový architektonický prvek, ale nemohl na nic přijít. Jednou ale procházel pohřebištěm a uviděl jeden dětský hrob. V té době se podle tradice po smrti dítěte na jeho hrob nanosily všechny jeho hračky. Uviděl tedy košík s hračkami prorostlý bodláčím a viděl v něm inspiraci pro novou hlavici.
-->

---

<!--
_class: image
-->

![bg](./images/diuv-chram.jpg)

# Chrám Dia Olympského – Athény, Řecko

---

<!--
_class: section-title
-->

# Athénská akropolis

---

# Athénská akropolis

* nejznámější akropole na světě
* politické, náboženské a kulturní centrum
* původně mykénský hrad
* budována ve 13. až 5. st. př. n. l.
* Parthenón – hlavní chrám
* od r. 1987 na seznamu UNESCO

<!--
Athénská akropolis je nejznámější akropolí na světě.

Tvořila politické, náboženské i kulturní centrum Řecka.

Akropole je vlastně opevněné návrší v Řeckých městech s vladařským palácem a chrámy.

Athénská akropole byla postavena na výrazném skalním pahorku.

Původně zde stál mykénský hrad. V 8. století př. n. l. byl na místě mykénského paláce vybudován dřevěný chrám bohyně Athény, později nahrazený kamenným chrámem. Po vítězství u Marathonu byla v 5. stol. př. n. l. plošina návrší vyrovnána bloky vápence a na vrcholu vystavěn Parthenón.

V 5. stol. př. n. l. byly také dostavěny ostatní chrámy Akropole.

Se svým hlavním chrámem Parthenónem tvoří dominantu Athén dodnes.

V roce 1987 byla zapsána na Seznam světového kulturního dědictví UNESCO.
-->

---

<!--
_class: image
-->

![bg](./images/akropolis.jpg)

# Akropolis v Athénách, Řecko

---

<!--
_class: image
-->

![bg](./images/akropolis-2.jpg)

# Akropolis v Athénách, Řecko

---

<!--
_class: section-title
-->

# Divy světa

---

# Feidiův Zeus v Olympii

* asi 12 metrů vysoká socha Dia
* vytesána 433 př. n. l.
* sochař Feidiás
* později přenesena do Konstantinopole
* zničena při požáru
* vyrobena ze slonoviny a zlata

![bg left:38%](./images/feidiuv-zeus.jpg)

<!--
Diova socha v Olympii byla asi 12 metrů vysoká socha, vysochaná v roce 433 př. n. l. Vytvořil jí sochař Feidiás.

V roce 394 n. l.byla přenesena do Konstantinopole, kde byla pravděpodobně zničena při požáru.

Byla vyrobena ze slonoviny a zlata a zdobena byla drahokamy.
-->

---

# Artemidin chrám v Efesu

* vybudován cca. 550 př. n. l.
* iónský sloh
* centrum bohyně Artemis
* jeden z největších řeckých chrámů starověku
* podpálen ve 4. stol. př. n. l.
* později obnoven, zanikl v pozdním starověku

<!--
Artemidin chrám v Efesu byl vybudován kolem roku 550 př. n. l v iónském slohu.

Jednalo se o jedno z center bohyně lovu Artemis a o jeden z největších starověkých řeckých chrámů.

V roce 356 př. n. l. chrám podpálil žhář Hérostratos se záměrem zapsat se do historie.

Obnoven byl pod dobytí Efesu Alexandrem Velikým, s nástupem křesťanství ale ztratil význam a postupně zanikl.
-->

---

<!--
_class: image
-->

![bg](./images/artemidin-chram-virt.jpg)

# Artemidin chrám (počítačová rekonstrukce) – Efesos, Turecko

---

<!--
_class: image
-->

![bg](./images/artemidin-chram.jpg)

# Artemidin chrám – Efesos, Turecko

---

# Mauzoleum v Halikarnassu

* vybudováno ve 4. stol př. n. l.
* současné Bodrum v Turecku
* iónský sloh
* zaniklo ve 12. stol. n. l.

![bg right:40% 100%](./images/mauzoleum-halikarnass-paint.png)

<!--
Mauzoleum v Halikarnassu byla hrobka vybudovaná Peršanem Mausólem v letech 353-350 př. n. l.

Nachází se v Halikarnassu – dnešním Bodrumu v Turecku.

Bylo postaveno v iónském slohu.

Ve 12. století našeho letopočtu byla stavba poškozena zemětřesením a následně rozebrána křižáky, kteří její zbytky využili na stavbu pevnosti proti Turkům.
-->

---

<!--
_class: image
-->

![bg](./images/mauzoleum-halikarnass-virt.jpg)

# Mauzoleum (počítačová rekonstrukce) – Bodrum, Turecko

---

<!--
_class: image
-->

![bg](./images/mauzoleum-halikarnass.jpg)

# Mauzoleum – Bodrum, Turecko

---

# Rhodský kolos

* bronzová socha boha Hélia
* vznikl 304-292 př. n. l.
* postaven obyvateli ostrova
* 30 metrů na výšku
* zničen zemětřesením

![bg left:44% 100%](./images/rhodsky-kolos-paint.jpg)

<!--
Rhodský kolos byla bronzová socha řeckého boha Hélia, postavená u přístavu na ostrově Rhodos.

Postaven v letech 304-292 př. n. l. obyvateli ostrova, jako poděkování bohu za odražení invaze makedonské armády.

Socha měřila 30 metrů a stala se tak nejvyšší sochou starověku.

Ne všichni Rhoďané ale sochu obdivovali, někteří si mysleli, že by takto velká socha mohla boha rozzlobit, a to tak, že by ji společně s ostrovem zničil.

Podle některých verzí socha stála rozkročená nad přístavem a mezi nohama jí proplouvaly lodě. Nohy by ale musela 12 metrů od sebe a to by se v kyčlích rozlomila.

V roce 226 př. n. l. došlo k zemětřesení, socha praskla v kolenou a spadla. Zbytky sochy později odvezli Arabové a pravděpodobně roztavili.
-->

---

<!--
_class: image
-->

![bg](./images/rhodsky-kolos-virt.jpg)

# Rhodský kolos (počítačová rekonstrukce) – Rhodos

---

# Maják na ostrově Faru

* postaven 279 př. n. l.
* původní podoba neznámá
* jediné dobové vyobrazení na mincích
* výška 60 až 80 metrů – nejvyšší stavba starověku

![bg right:42% 100%](./images/majak-faros-mince.jpg)

<!--
Maják na ostrově Faros u alexandrijského přístavu byl postaven v roce 279 př. n. l.

Původní podoba stavby je neznámá a nelze ji rekonstruovat, protože se z ní nic nedochovalo a neexistují ani spolehlivé popisy.

Nejznámější dobovou kresbou je jeho znázornění na římských mincích z 2. stolení našeho letopočtu.

Byla to nejvyšší stavba starověkého světa. Výška se odhaduje mezi 60 až 80 metrů.
-->

---

<!--
_class: image
-->

![bg](./images/majak-faros-virt.jpg)

# Maják na ostrově Faru (počítačová rekonstrukce) – Alexandrie, Egypt

---

<!--
_class: main-title
footer: ""
-->

# Děkujeme za pozornost!

---

# Zdroje

- http://vygosh.cz/de-recko.html
- https://recko.svetadily.cz/clanky/Recka-architektura
- https://cs.wikipedia.org/wiki/Architektura_starov%C4%9Bk%C3%A9ho_%C5%98ecka
- https://cs.wikipedia.org/wiki/Akropolis
- Periklovo Řecko, Jan Bouzek a Iva Ondřejová *(ISBN 80-204-0083-4)*
- https://cs.wikipedia.org/wiki/Artemidin_chr%C3%A1m_v_Efesu
- https://cs.wikipedia.org/wiki/Feidi%C5%AFv_Zeus_v_Olympii
- https://cs.wikipedia.org/wiki/Mauzoleum_v_Halikarnassu
- https://cs.wikipedia.org/wiki/Rhodsk%C3%BD_kolos
- https://cs.wikipedia.org/wiki/Maj%C3%A1k_na_ostrov%C4%9B_Faros
